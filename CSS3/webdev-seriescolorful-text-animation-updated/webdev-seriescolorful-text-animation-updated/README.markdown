# #webdev series - Colorful text animation #updated
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/hendrysadrak/pen/VLMOMJ](https://codepen.io/hendrysadrak/pen/VLMOMJ).

 Colorful text animation

Fluid and configurable colorful text animation module made with scss.
Use it wherever you want but please give credit ;)

In use http://hendrysadrak.com