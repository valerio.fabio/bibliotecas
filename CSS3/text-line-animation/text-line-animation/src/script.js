var win = window,
	doc = document,
	body = doc.body;

body.style.overflow = 'hidden';

var ww = win.innerWidth;
var wh = win.innerHeight;

var a = TweenMax;

function rand(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}

var paths = doc.getElementsByTagName('path');

paths = [].slice.call(paths);

// paths.forEach(function(path, i){
	
// 	var prev = i == 0 ? false : paths[i-1];

// 	var len = path.getTotalLength(),
// 		ranum = len / rand(0, 3),
// 		offset = rand(0, 400);
	

// 	path._offset = offset;
// 	path.style.strokeDasharray = ranum;
// 	path.style.strokeDashoffset = path._offset;
// });

// function anim(){
	 
// 	paths.forEach(function(path){
		
// 		path._offset += 20;
		
// 		path.style.strokeDashoffset = path._offset;
		
// 	});
	
// 	requestAnimationFrame(anim);
// }

// anim();

// -------------

// paths.forEach(function(path, i){

// 	var len = Math.ceil(path.getTotalLength());

// 	path._len = len;
// 	path.style.strokeDasharray = len;
// 	path.style.strokeDashoffset = len;
// });

// var tl = new TimelineMax({paused: true, repeat: -1});

// tl.staggerTo(paths, 1, 
// 	{
// 		strokeDashoffset: 0,
// 		ease: Power4.easeOut
// 	}, 0.1);

// tl.staggerTo(paths, 1, 
// 	{
// 		cycle: {
// 			strokeDashoffset: function(idx){
// 				return -paths[idx]._len;
// 			}
// 		},
// 		ease: Power4.easeIn
// 	}, 0.1, 2);

// tl.play();


// --------------


paths.forEach(function(path, i){

	var len = Math.ceil(path.getTotalLength());

	path._len = len;
	path.style.strokeDasharray = len;
	path.style.strokeDashoffset = len;
});

var alt = true;

paths = paths.reverse();

function pathsReset(){
	
	paths.forEach(function(path, i){
		setTimeout(() => {
			
			a.to(path, 1, {strokeDashoffset: 0})
			
			if (i == paths.length-1) {
				if (alt) {
					setTimeout(pathsOut, 1500);
				} else {
					setTimeout(pathsIn, 1500);
				}
				alt = !alt;
			};
			
		}, 20 * i);
	});
}

function pathsOut(){
	
	paths.forEach(function(path, i){
		setTimeout(() => {
			
			a.to(path, 1, {strokeDashoffset: -path._len})
			
			if (i == paths.length-1) pathsReset();
			
		}, 20 * i);
	});
}

function pathsIn(){
	
	paths.forEach(function(path, i){
		setTimeout(() => {
			
			a.to(path, 1, {strokeDashoffset: path._len})
			
			if (i == paths.length-1) pathsReset();
			
		}, 50 * i);
	});
}

pathsReset();