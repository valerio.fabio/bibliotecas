# Transmission: Glowing Text Animation
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/StephenScaff/pen/oLBqmw](https://codepen.io/StephenScaff/pen/oLBqmw).

 A little glowing text animation.
First, some js to wrap each letter in a span.
Then a keyframe animation, with animation-delay mixin, lights up each letter in succession. 