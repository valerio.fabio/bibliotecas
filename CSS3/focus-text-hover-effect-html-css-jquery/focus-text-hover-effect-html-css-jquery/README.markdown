# Focus Text Hover Effect | HTML+   CSS + jQuery
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/CameronFitzwilliam/pen/JJRjMa](https://codepen.io/CameronFitzwilliam/pen/JJRjMa).

 Hover jQuery| HTML | CSS effect for text.

*Updated 19/10/2017
Applied a DRY approach, removed unnecessary code.

Influenced by:
https://dribbble.com/shots/2076176-Focus