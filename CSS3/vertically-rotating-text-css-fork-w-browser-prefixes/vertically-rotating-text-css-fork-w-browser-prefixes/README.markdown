# Vertically rotating text css [FORK] w/ browser prefixes
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/JacobStone/pen/dyEvl](https://codepen.io/JacobStone/pen/dyEvl).

 This is a fork of Miloš Rujević's wonderful codebit. I've changed the font to Montserrat and added CSS3 browser prefixes so the animation works right out of the box across browsers. 

Should work with Firefox, Chrome, Safari, Opera 