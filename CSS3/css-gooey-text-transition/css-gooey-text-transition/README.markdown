# CSS Gooey Text Transition
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/mikegolus/pen/MBRKyB](https://codepen.io/mikegolus/pen/MBRKyB).

 A very simple and versatile text morphing effect with a couple editable parameters. 