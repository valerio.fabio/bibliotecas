# Shattering Text Animation
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/ARS/pen/pjypwd](https://codepen.io/ARS/pen/pjypwd).

 GSAP text animation. SVG path shattering. Slow motion on hover.