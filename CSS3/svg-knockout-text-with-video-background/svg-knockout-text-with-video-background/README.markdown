# SVG Knockout Text with Video Background
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/Yuschick/pen/BdYooL](https://codepen.io/Yuschick/pen/BdYooL).

 This demo explores creating knockout text/paths in SVG and looping a YouTube video as the fill.

My personal 31 Nights of Horror series continues this year and this was built as the teaser landing page. Follow the calendar here: https://yuschick.github.io/31-Nights-of-Horror-2017