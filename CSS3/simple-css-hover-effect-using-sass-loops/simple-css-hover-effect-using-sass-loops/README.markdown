# Simple CSS Hover Effect using Sass Loops
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/FUGU22/pen/YxEojN](https://codepen.io/FUGU22/pen/YxEojN).

 Simple little hover animation. Sass loops make staggering animation delays really easy to do...you can get a lot of mileage out of them ;D