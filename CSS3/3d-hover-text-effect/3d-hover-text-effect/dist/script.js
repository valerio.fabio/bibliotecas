function toScreenXY( position, camera, div ) {
	var pos = position.clone();
	projScreenMat = new THREE.Matrix4();
	projScreenMat.multiplyMatrices( camera.projectionMatrix, camera.matrixWorldInverse );	
	pos.applyProjection(projScreenMat);
	return { x: ( pos.x + 1 ) * canvas_width / 2,
	     y: ( - pos.y + 1) * canvas_height / 2  };
}


function pointInCircle(point,target, radius) {
  var distsq = (point.x - target.x) * (point.x - target.x) + (point.y - target.y) * (point.y - target.y) + (point.z - target.z) * (point.z - target.z);
  // returns bool , distance to target origin 
  return [distsq <= radius * radius * radius,distsq];
}
//_____________________________________________

var time = 0;
var codepen;
var line;
var ref_codepen = [];

var main_color = 0xef373e;
var dark_main_color = 0x000000;
var canvas_height = window.innerHeight;
var canvas_width = window.innerWidth;
var mouse_vector = new THREE.Vector3(0,0,0);
var particles;

var widthHalf = canvas_width / 2;
var heightHalf = canvas_height / 2;

var mouse = {
	x : 0,
	y : 0,
	three : new THREE.Vector3(0,0,0)
}
//_____________________________________________

var scene = new THREE.Scene();

//_____________________________________________
var camera = new THREE.PerspectiveCamera( 75, canvas_width/canvas_height, 0.1, 2000 );
  
  camera.position.set(0,5,150);
  scene.add(camera);
//_____________________________________________

var renderer = new THREE.WebGLRenderer({ alpha: true,antialias: true }); 
    renderer.setSize( canvas_width, canvas_height );
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    document.body.appendChild( renderer.domElement );
    document.addEventListener('mousemove',function(event){
      mouse.x = event.pageX;
      mouse.y = event.pageY;


      mouse_vector = new THREE.Vector3();

      mouse_vector.set(
        ( mouse.x / canvas_width ) * 2 - 1,
        - ( mouse.y / canvas_height ) * 2 + 1,
        0.5 );

      mouse_vector.unproject( camera );

      var dir = mouse_vector.sub( camera.position ).normalize();
      var distance = - camera.position.z / dir.z;
          mouse.three = camera.position.clone().add(dir.multiplyScalar( distance ));

    });


//_____________________________________________
window.onresize = function(){
  canvas_height = window.innerHeight;
  canvas_width = window.innerWidth;
  camera.aspect = canvas_width / canvas_height;
  camera.updateProjectionMatrix();
  renderer.setSize( canvas_width, canvas_height );
}
//_____________________________________________
    controls = new THREE.OrbitControls( camera );

controls.damping = 0.001;
controls.target.set(0,0,0);
controls.maxDistance = 500;
controls.update();
//_____________________________________________

//____________________________ Mesh Loader
var point_codepen = [];
var cMaterial = new THREE.PointsMaterial({
      color: main_color,
      size: .25,
      sizeAttenuation : true,
      transparent:true,
      depthTest: true,
      depthWrite: true,
      alphaTest: .8
});
//_____________________________________________ MAterials
var font_material = new THREE.MeshBasicMaterial({
    transparent : true
});
var shadowMaterial = new THREE.MeshBasicMaterial({
  transparent : true
});
var line_material = new THREE.LineBasicMaterial({
	transparent : true
});

//_____________________________________________ Loader

var loader = new THREE.JSONLoader();
    loader.load('https://s3-us-west-2.amazonaws.com/s.cdpn.io/61062/codepen.json',function(geometry,material){
    
		codepen = new THREE.Mesh(geometry,new THREE.MeshFaceMaterial([font_material,shadowMaterial]));
    scene.add(codepen);
		
		// SINGLE ROW
		codepen.geometry.vertices.forEach(function(p,index){
				point_codepen.push({
					x : p.x ,
					y : p.y ,
					z : p.z 				
        });	
		});
		
		var particles_GEO = new THREE.Geometry();
		var line_GEO = new THREE.Geometry();
      
		for(var p=0;p<point_codepen.length;p++){
				particles_GEO.vertices.push(point_codepen[p]);
			  line_GEO.vertices.push(point_codepen[p]);
		}
      
    //_________________ 
			particles = new THREE.Points(particles_GEO,cMaterial);
			particles.original_geometry = point_codepen;
			scene.add(particles);

    //_________________ 
			line = new THREE.Line(line_GEO, line_material);
			//scene.add(line);
});

//_____________________________________________ DRAW

var attraction = {
	x : -.000005,
	y : -.000005,
	z : -.000005
}

var hue = 0;

function draw(time){
	var inCircle = [];
	  hue+= .5;
  	if(typeof particles != "undefined"){
		particles.geometry.vertices.forEach(function(p,index){

		var checker = pointInCircle(codepen.geometry.vertices[index],mouse.three, 10);
		var fac = 0;
		
	
		
		if(checker[0] == true){
			inCircle.push(p);
			if(p.x > mouse.three.x){
				p.x -= attraction.x;
				line.geometry.vertices[index].x -= Math.sin(time * attraction.x) * Math.cos(.0025 * checker[1]) * 2 ;
			}
			else{
				p.x += attraction.x;
				line.geometry.vertices[index].x += Math.sin(time * attraction.x) * Math.cos(.0025 * checker[1]) * 2 ;
			}
			if(p.y > mouse.three.y){
				p.y -= attraction.y;
				line.geometry.vertices[index].y -= Math.sin(time * attraction.y) * Math.cos(.0025 * checker[1]) * 2 ;
			}
			else{
				p.y += attraction.y;
				line.geometry.vertices[index].y += Math.sin(time * attraction.y) * Math.cos(.0025 * checker[1]) * 2 ;
			}
			if(p.z > mouse.three.z){
				p.z -= attraction.z;
				line.geometry.vertices[index].z -= Math.sin(time * attraction.z) * Math.cos(.0025 * checker[1]) * 2 ;
			}
			else{
				p.z += attraction.z;
				line.geometry.vertices[index].z += Math.sin(time * attraction.z) * Math.cos(.0025 * checker[1]) * 2 ;
			}
		}
		else{

			p.x = line.geometry.vertices[index].x = codepen.geometry.vertices[index].x ;
			p.y = line.geometry.vertices[index].y = codepen.geometry.vertices[index].y ;
			p.z = line.geometry.vertices[index].z = codepen.geometry.vertices[index].z ;
		}
});
		
  codepen.material.materials.forEach(function(m,index){
			m.opacity = 1- .003 * inCircle.length;
			if(m.opacity<=0){
				m.visible = false;
			}
			else{
				m.visible = true;
			}

			if(index == 0){
				m.color = new THREE.Color('hsl('+hue+',100%,50%)');
			}
			if(index == 1){
				m.color = new THREE.Color('hsl('+hue+',100%,70%)');
			}
		});

      particles.material.opacity = .005 * inCircle.length;
      particles.material.color = new THREE.Color('hsl('+hue+',100%,50%)');


      line.material.opacity = .0005 * inCircle.length;
      line.material.color = new THREE.Color('hsl('+hue+',100%,80%)');

      particles.geometry.verticesNeedUpdate = true;
      line.geometry.verticesNeedUpdate = true;
	  }
  }


//_____________________________________________

function animation(time){
  requestAnimationFrame(animation);
  draw(time);
  renderer.render(scene, camera);
}

//_____________________________________________ 

animation(time);