# Gooey text background with SVG filters
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/ines/pen/NXbmRO](https://codepen.io/ines/pen/NXbmRO).

 Example of using a gooey SVG filter to create smooth edges around inline text with a background. More details: https://ines.io/blog/newwwyear-2017