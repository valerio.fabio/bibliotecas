# SVG Text Underline
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/iam_aspencer/pen/qvNPBv](https://codepen.io/iam_aspencer/pen/qvNPBv).

 Use an SVG to highlight a single word within a block of text. The size of the SVG will flex to fit the word and a <strong> tag is used to handle semantics.